const form = document.forms.mainform;

form.addEventListener('submit', async (ev) => {
    ev.preventDefault();
    const formData = new FormData(ev.target);
    const { data } = await axios.post('/save', formData);
    console.log(data)

    const justAdd = document.querySelector('.justadd')
    justAdd.innerHTML = `
    <ul>
        <li> title - ${data.data.title === '' ? 'empty' : data.data.title}</li>
        <li> description - ${data.data.description === '' ? 'empty' : data.data.description}</li>
        <li> author - ${data.data.author === '' ? 'empty' : data.data.author}</li>
    </ul>`
    
    
});

