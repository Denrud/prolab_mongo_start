const mongoose = require('mongoose');

const url = 'mongodb://localhost:27017/posts_storage';

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

mongoose.connect(url, options);
const db = mongoose.connection;

db.on('error', (err) => {
    console.log('DB err:', err);
});
db.once('open', () => {
    console.log('connect to DB');
});
db.once('close', () => {
    console.log('Colse conneced to DB');
});

