const PostModel = require("../models/post");

const modelSave = async ( data ) => {
    const post = new PostModel;
    post.title = data.title;
    post.author = data.author;
    post.author.name = data.author;
    post.author.email = '';
    post.date = new Date();
    const rec = await post.save();
};
module.exports = modelSave;